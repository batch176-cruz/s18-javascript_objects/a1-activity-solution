function Pokemon(name, level) {
	//properties
	this.name = name;
	this.level = level;
	this.health = 3 * level;
	this.attack = level;

	//methods
	this.tackle = function(target) {
		console.log(this.name + " tackled " + target.name);
		console.log(target.name + "'s health is now reduced to " + (target.health - this.attack));

		target.health = target.health - this.attack;
		
		if (target.health < 5) {
			target.faint();
		}
	};
	this.faint = function() {
		console.log(this.name + " fainted");
	}
};

let bulbasaur = new Pokemon("Bulbasaur", 15);
let squirtle = new Pokemon("Squirtle", 15);

